<?php
//Регистрирация службы фрамеворка
use Phalcon\Di\FactoryDefault;


//включение ошибок
error_reporting(E_ALL);

//установление констант
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH',BASE_PATH.'/app');

//подключаю autoload.php
require_once(__DIR__.'/../vendor/autoload.php');
$di=New FactoryDefault();
//обработка исключений
try {


//подключаем роутер, сервисы
  include APP_PATH.'/config/router.php';
  include APP_PATH.'/config/services.php';

    //получаем конфиг
    $config=$di->getconfig();

    include APP_PATH.'/config/loader.php';

    $application=new Phalcon\Mvc\Application($di);

  echo str_replace(["\n", "\r", "\t"], '',    $application->handle()->getContent());
  } catch (\Exception $e){
    echo '<b>'.$e->getMessage().'</b>';
    echo '<pre>'.$e->getTraceAsString().'</pre>';
}
