<?php
use Phalcon\Mvc\Model;
class Camera extends \Phalcon\Mvc\MongoCollection
{
	public function getSource()
	{
			return 'camers'; //Указываем имя колекции
	}
	public static function findNotRemoved()
	{
		return self::find([[
			'is_removed' => [
				 '$ne' => 'TRUE'
		]]]);
	}

	// public static function findnamePhoto()
	// {
	// 	return self::find([[
	// 		'namePhoto' => [
	// 			 '$ne' => 'TRUE'
	// 	]]]);
	// }

}
