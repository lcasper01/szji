<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

use Phalcon\Flash\Session as FlashSession;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

            return $volt;
        }
    ]);

    return $view;
});

/**
 * Detecting mobile devices
 */
// $di->setShared('mobiledetect', function () {
//     return new Mobile_Detect();
// });


/**
* MongoDB connection
*/
$di->set("mongo", function () {
    $config = $this->getConfig();

    $db_string = sprintf('mongodb://%s:%s', $config->mongodb->host, $config->mongodb->port);

    if (isset($config->mongodb->username) and isset($config->mongodb->password)) {
        $db_string = sprintf('mongodb://%s:%s@%s:%s',
                $config->mongodb->username,
                $config->mongodb->password,
                $config->mongodb->host,
                (string)$config->mongodb->port);
    }

    try {
        $mongo = new \Phalcon\Db\Adapter\MongoDB\Client($db_string);
        return $mongo->selectDatabase($config->mongodb->database);
    } catch (MongoConnectionException $e) {
        die('Failed to connect to MongoDB '.$e->getMessage());
    }
},
    true
);

$di->setShared("camers", function () {
     $mongo = $this->getMongo();
//     // var_dump( $mongo );die();
//     return [ 'title' => 'Ромашка' ];
 }
);

/**
* Collection Manager
*/
$di->set(
    "collectionManager",
    function () {
        return new Phalcon\Mvc\Collection\Manager;
    }
);

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */

// Set up the flash session service
$di->set(
    'flashSession',
    function () {
        return new FlashSession();
    }
);

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});
