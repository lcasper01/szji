<?php
$router=$di->getRouter(FALSE);

$router->add("/ShowCamers", 'Index::ShowCamers')->via(['GET']);
$router->addGet("/", 'Index::index')->via(['GET']);
$router->add("/AddCamers", 'Camer::AddCamers')->via(['GET', 'POST']);
$router->add("/Camers/Show", 'Camer::ShowCamers')->via(['GET']);
$router->add("/camera/delete/{id}", 'Camer::CameraDelete')->via(['GET']);
$router->add("/camera/edit/{id}", 'Camer::CameraEdit')->via(['GET','POST']);
$router->add("/camera/UploadPhoto", 'Camer::UploadPhotoCamer')->via(['GET', 'POST']);
$router->add("/image/UploadImage", 'Image::create')->via(['GET', 'POST']);
$router->add( '/camera_photo/{image_id}', 'Image::getImage' )->via( [ 'GET' ] );




$router->notFound( [ "controller" => "index", "action"     => "notfound", ] );

$router->handle();
