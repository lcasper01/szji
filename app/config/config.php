<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'site' => [
        'title' => 'Camomile',
    //    'style' => 'bootstrap.min.css',
        'description' => 'IT CookBook',
        'domain' => sprintf( '%s://%s', $_SERVER[ 'REQUEST_SCHEME' ], $_SERVER[ 'HTTP_HOST' ] ),
        'url' => sprintf( '%s://%s%s', $_SERVER[ 'REQUEST_SCHEME' ], $_SERVER[ 'HTTP_HOST' ], $_SERVER[ 'REQUEST_URI' ] )
    ],
    'mongodb' => [
        'host' => 'localhost',
        'port' => '27017',
        // 'username' => '',
        // 'password' => '',
        'database' => 'camers',
         'options' => [
             'db' => 'admin'
         ]
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'stylesDir'      => BASE_PATH . '/public/css/',
        // 'uploadsDir'     => BASE_PATH . '/uploads/',
        // 'avatarsDir'     => BASE_PATH . '/uploads/avatars/',

        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        // 'baseUri'        => preg_replace('/public([\/\\\\])$/', '', $_SERVER["PHP_SELF"]),
        'baseUri' => '/'
    ]
]);
