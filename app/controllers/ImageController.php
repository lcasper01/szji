<?php
use Phalcon\Http\Response;

class ImageController extends ControllerBase
{
  public function getImageAction()
  {
    $this->view->disable();
    $img_id = $this->dispatcher->getParam( 'image_id' );
    $image=Image::findById($img_id);
    $this->response->resetHeaders();
    $this->response->setStatusCode(200, 'OK');
    $this->response->setContentLength($image->size);
    $this->response->setContentType($image->mime);

    $content = $image->picture->getData();
    $this->response->setContent($content);
    // var_dump($image->picture);die();
    return $this->response->send();

  }

  public function createAction()
    {
      $this->view->setVar("title", "Загрузка фото камеры");
      if ($this->request->isPost()){
      $errror='Ошибка';
      $allowExts = [ 'jpg', 'jpeg', 'png', 'gif' ];
      $allowTypes = [ 'image/jpeg', 'image/jpg', 'image/png', 'image/gif' ];
      // $this->view->disable();
      // загрузил ли файл
      if (!$this->request->hasFiles()) {
            return $error;
      }

      if (!$file = ($this->request->getUploadedFiles())[0]) {
        return $error;
      }

      if (!$file_tmp = $file->getTempName() or !file_exists($file_tmp)) {
        return $error;
      }

      if (!$file_ext = $file->getExtension()
          or !in_array(strtolower($file_ext), $allowExts)) {
          return $error;
      }

      if (!$file_type = $file->getRealType()
          or !in_array($file_type, $allowTypes)) {
          return $error;
      }
      if (!$file_data = new MongoDB\BSON\Binary(file_get_contents($file->getTempName()), MongoDB\BSON\Binary::TYPE_GENERIC)) {
          return $error;
          }
      $image=New Image();
      $image->mime = $file_type;
      $image->size = $file->getSize();
      $image->picture = $file_data;

      if (!$image->save()) {
          return $error;
      }

      }
      return $this->view->pick('add\CameraPhotoUpload');
    }


}
