<?php
use Phalcon\Http\Request;
use Phalcon\Http\Response;

class CamerController extends ControllerBase
{
  public function AddCamersAction()
  {
        $this->view->setVar("title", "Добавление камер");
      if ($this->request->isPost()){
        $camera=New Camera();
        $camera->nameCam = trim($this->request->getPost('nameCam'));
        $camera->IPCam = trim($this->request->getPost('IPCam'));
        $camera->macCam = trim($this->request->getPost('macCam'));
        $camera->IPswitch = trim($this->request->getPost('IPswitch'));
        $camera->portNumber = trim($this->request->getPost('portNumber'));
        $camera->namePhoto = trim($this->request->getPost('namePhoto'));
        $camera->save();
        }


        return $this->view->pick('add\addcam');
  }

  public function ShowCamersAction()
  {
    $this->view->setVar("title", "Список камер");
    $camers = Camera::findNotRemoved();
    $image=Image::find();
    $this->view->setVar("image", $image);
    $this->view->setVar("camera", $camers);

    return $this->view->pick('show\CamersShow');
  }

  public function CameraDeleteAction()
  {
    $this->view->setVar("title", "Удаление");
    $cam_id = $this->dispatcher->getParam( 'id' );
    $camera=Camera::findById($cam_id);
    $camera->is_removed='TRUE';
    $camera->save();
    return $this->view->pick('index\index');
  }

    public function  CameraEditAction()
    {
    $this->view->setVar("title", "Редактирование");
    $cam_id = $this->dispatcher->getParam( 'id' );
    $camera=Camera::findById($cam_id);

    if ($this->request->isPost()){
      $camera->nameCam = trim($this->request->getPost('nameCam'));
      $camera->IPCam = trim($this->request->getPost('IPCam'));
      $camera->macCam = trim($this->request->getPost('macCam'));
      $camera->IPswitch = trim($this->request->getPost('IPswitch'));
      $camera->portNumber = trim($this->request->getPost('portNumber'));
      $camera->namePhoto = trim($this->request->getPost('namePhoto'));
      $camera->save();
      $this->response->redirect('/Camers/Show');
      }
      $this->view->setVar("camera", $camera);
      $this->view->setVar("id", $cam_id);
    return $this->view->pick('edit\CameraEdit');

    }



  public function UploadPhotoCamerAction()
    {
      $this->view->setVar("title", "Загрузка фото камеры");
      if ($this->request->isPost()){
      $allowed_filetypes = array('.jpg','.gif','.bmp','.png'); // Допустимые типы файлов
      $max_filesize = 524288; // Максимальный размер файла в байтах (в данном случае он равен 0.5 Мб).
      $upload_path = BASE_PATH . '/var/camera_photo/'; // Папка, куда будут загружаться файлы .
      $filename = $_FILES['userfile']['name']; // В переменную $filename заносим имя файла (включая расширение).
      $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); // В переменную $ext заносим расширение загруженного файла.

      if(!in_array($ext,$allowed_filetypes)) // Сверяем полученное расширение со списком допутимых расширений.
      die('Данный тип файла не поддерживается.');

      if(filesize($_FILES['userfile']['tmp_name']) > $max_filesize) // Проверим размер загруженного файла.
      die('Фаил слишком большой.');
      // var_dump($upload_path);
      // die();
      if(!is_writable($upload_path)) // Проверяем, доступна ли на запись папка.
     die('Невозможно загрузить фаил в папку. Установите права доступа - 777.');

      // Загружаем фаил в указанную папку.
      move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $filename);
      }
      return $this->view->pick('add\CameraPhotoUpload');
  }

}
