<?php
class IndexController extends ControllerBase
{
  public function indexAction()
  {
    $title="СЗЖИ";
    $this->view->setVar("title", $title);
  return $this->view->pick('index/index');
  }

  public function notfoundAction()
  {
    return $this->view->pick( 'index/404' );
  }

}
