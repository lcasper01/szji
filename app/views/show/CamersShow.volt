  {% if camera is defined and camera is iterable %}
        <table border=1 align=center class="table table-hover table-dark">
          <tr>
            <th>Наименование</th>
            <th>IP камеры</th>
            <th>MAC адресс</th>
            <th>IP коммутатора</th>
            <th>№ порта</th>
            <th>Фото</th>
            <th>Действие</th>
          </tr>
              {% for single_camera in camera %}
                    <tr>
                      <td>{{ single_camera.nameCam }}</td>
                      <td>{{ single_camera.IPCam }}</td>
                      <td>{{ single_camera.macCam }}</td>
                      <td>{{ single_camera.IPswitch }}</td>
                      <td>{{ single_camera.portNumber }}</td>
                      <td><img src="/camera_photo/5b83d71503ac650f440076b6" width="150" height="150" alt="тык"></td>
                      <td><a href="/camera/delete/{{ single_camera._id }}">Удалить</a><br/>
                      <a href="/camera/edit/{{ single_camera._id }}">Редактировать</a></td>
                    </tr>
              {% endfor %}
        </table>
  {% endif %}
